<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
	<link rel="stylesheet" type="text/css" href="css.css" />
	<!--Title-->
	<title>Kiszony Ogórek</title>
</head>

<body>
	 <div id="container">
		<div id="header">
			<!--Header-->
			<h1>Pickled Cucumbers</h1>
			<h2>Page that does love pickled cucumbers</h2>
		</div>
		<!--Navigation Bar-->
		<ul id="nav">
			<li><a title="Main Page" href="main.html">Main</a></li>
			<li><a title="About" href="about.html">About</a></li>
			<li><a title="Gallery" href="gallery.html">Gallery</a></li>
			<li><a title="Offer" href="offer.php">Offer</a></li>
			<li><a title="JS" href="js.html">JS</a></li>
			<li><a title="Contact" href="contact.html">Contact</a></li>
		</ul>
		<h3> Please enter the amount of cucumbers that you want to have in your jar: </h3>

		<div class="w3-content">
			<form method="post" action="">
				Quantity:
				<br>
				<input type="number" name="number" required><br> Message:
				<br>
				<input type="text" name="text" required><br>
				<br>
				<input type="submit" name="addCucumb" value="Submit"><br>
				<br>
			</form>

<?php
include_once("sqlitedatabase.php");
include_once("ogorektable.php");

// Define configuration
//define("DB_HOST", "localhost");
//define("DB_USER", "id295697_admin");
//define("DB_PASS", "ogorek");
//define("DB_NAME", "id295697_ogorekdb");
define("DB_NAME", "ogorekdb.sqlite");
define("OGOREK_TABLE", "ogorek_tbl");

$dbh = new SqliteDatabase(DB_NAME);
$ogorekTable = new OgorekTable($dbh, OGOREK_TABLE);

$notify = "<br>";
if($_SERVER["REQUEST_METHOD"] == "POST") {
	if (isset($_POST['addCucumb'])) {
		$number = $_POST['number'];
		$text = $_POST['text'];

		$rowId = $ogorekTable->insert($number, $text);
		$notify = "Added: row id: $rowId Quntity: <b> $number </b> Message:<b> $text </b> <br>";
	}
	else if (isset($_POST['deledeCucumb'])) { 
		$rowId = $_POST['rowId'];
		$ogorekTable->deleteRow($rowId);

		$notify = "Deleted row id:' $rowId ' <br>";
	}
}

echo $notify;

$allRows = $ogorekTable->getAllRows();

//print_r($allRows);
?>

				<div  align=center>
				<table>
					<tr>
						<th>Number</th>
						<th>Message</th>
						<th></th>
					</tr>
					<?php foreach ($allRows as $row) { ?>
					<form method="post" action="">
						<tr id=row_<?php $row['Id']?> >
							<td><?php echo $row['Number']; ?></td>
							<td><?php echo $row['Text']; ?></td>
							<td>
								<input type="hidden" name="rowId" value=<?php echo $row['Id'] ?> >
								<input type="submit" name="deledeCucumb" value="Delete">
							</td>
						</tr>
					</form>
					<?php } ?>
				</table>
				</div>
			</div>

			<!--Footer-->
			<div id="footer">
				<p>Copyright © 2016 - Purdenko Iryna</p>
			</div>
	</body>
</html>
