<?php
include_once("database.php");

class MysqlDatabase{
    private $host = "";
    private $user = "";
    private $pass = "";
    private $dbname = "";
 
    public function __construct($host, $user, $pass, $dbname){
        $this->host = $host;
        $this->user = $user;
        $this->pass = $pass;
        $this->dbname = $dbname;

        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
        $options = array(
            PDO::ATTR_PERSISTENT    => true,
            PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION
        );

        try{
           $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        }
        catch(PDOException $e){
            $this->error = $e->getMessage();
            echo 'Could not connect to database: error: ' . $this->error . '<br>';
        }
    }   
}
?>
