<?php

include_once ("table.php");

class OgorekTable extends Table {

    public function __construct($db, $name){
        parent::__construct($db, $name);

        $db->query('CREATE TABLE IF NOT EXISTS '. $this->name . '( Id INTEGER, Number INTEGER, Text Text, PRIMARY KEY(id ASC))');
        $db->execute();
    }

    public function insert($number, $text)
    {
        $this->db->query('INSERT INTO ' . $this->name . ' (Number, Text) VALUES (:number, :text)');
        $this->db->bind(':number', $number, PDO::PARAM_INT);
        $this->db->bind(':text', $text, PDO::PARAM_STR);
        $this->db->execute();
		return $this->db->lastInsertId();
    }

    public function deleteRow($rowId)
    {
        $this->db->query('DELETE FROM ' . $this->name . ' WHERE  Id  = :id');
        $this->db->bind(':id', $rowId);
        $this->db->execute();
    }

    public function getAllRows()
    {
        $this->db->query('SELECT * FROM ' . $this->name);
        return $this->db->resultset();
    }
}
?>
