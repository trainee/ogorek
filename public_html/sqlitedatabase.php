<?php
include_once("database.php");

class SqliteDatabase extends Database {
    private $dbFile = "";
 
    public function __construct($dbFile){
        parent::__construct();
        
        $this->dbFile =$dbFile ;
        $dsn = 'sqlite:' . $dbFile;
        
        try{
           $this->dbh = new PDO($dsn);
           //$this->dbh->setAttribute(PDO::ATTR_PERSISTENT, true);
           //$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e){
            $this->error = $e->getMessage();
            echo 'Could not connect to database: error: ' . $this->error . '<br>';
        }
    }
}
?>
