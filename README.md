# README #

### What is this repository for? ###

* This is a study project consisting in development a demo web site about pickled cucumbers.

### How do I get set up? ###

* The web site should be run on the web hosting server or it could be run locally on PC using web server app, f.e. EasyPHP Devserver.
* No special configuration (except configuring a web server) is requiered.
* SQLite data base is used, so there is no need to configure data base on the server side.

### Site structure ###

Site consists of the following pages:

1. Main page. This page demonstrates skills of using different CSS styles, HTML elements and JavaScripts by means of impelemnting active image scroller. Also different ways of using style formatting is shown: including local CSS file, reference to the stlyesheet in the Internet, style sections in the html file and stlye attributes of HTML elements are used here.

2. About page. A simple page just to demonstrate navigation between pages. Simple style formating is used.

3. Gallery page. This page demonstrates how to use images from different sources and simple JavaScript insertions to handle image selections.

4. Offer page. Here PHP and SQL is demonstrated. This page is written in PHP. A simple SQLite data base is created and modified there (adding new items, removing existing items). SQlite PDO driver was used to allow easy migration to any other kind of data base with the minimum code modification.

5. JS page. The aim of this page is demostration of extensive work with forms and JavaScript. In the background of the two forms all math calculations are implemented in JavaScript.

6. Contact page. Again, a simple page just to add content for the site.
